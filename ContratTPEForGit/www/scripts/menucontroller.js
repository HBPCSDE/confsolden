
contrattpe.controller('menucontroller', function ($scope, $location, $timeout, $mdSidenav, $log,  $mdDialog, $mdMedia) {

    //$scope.toggleLeft = buildDelayedToggler('left');
	//$scope.toggleLeft = buildDelayedToggler('left');
	$scope.toggleLeft = buildToggler('left');
	$scope.toggleRight = buildToggler('right');

	$scope.isContratHidden = true;
    
	$scope.isCalendarHidden =false;

	$scope.isActiviteCliquable = false;
	$scope.isActiviteCurrent = true;
	$scope.isActiviteDisabled = false;
	
	$scope.isDetailCliquable = false;
	$scope.isDetailCurrent = false;
	$scope.isDetailDisabled = false;

	$scope.isOffreliquable = false;
	$scope.isOffreCurrent = false;
	$scope.isOffreDisabled = false;


	$scope.isFinDisabled = false;
	$scope.isFinDisabled = false;
	$scope.isFinDisabled = false;
    
	$scope.desactiveAll = function () {
	    $scope.isActiviteCliquable = false;
	    $scope.isActiviteCurrent = false;
	    $scope.isActiviteDisabled = false;

	    $scope.isDetailCliquable = false;
	    $scope.isDetailCurrent = false;
	    $scope.isDetailDisabled = false;

	    $scope.isOffreliquable = false;
	    $scope.isOffreCurrent = false;
	    $scope.isOffreDisabled = false;


	    $scope.isFinDisabled = false;
	    $scope.isFinDisabled = false;
	    $scope.isFinDisabled = false;
	};

	$scope.quitContrat = function () {
	    $scope.isContratHidden = true;
	    $scope.basculeAccueil();

	}

	$scope.etapeActivite = function () {
	    if (!$scope.isActiviteDisabled) {
	        $scope.desactiveAll();
	        $scope.isActiviteCurrent = true;
	        $location.path('/activite');
	    }
	    
	};

	$scope.etapeDetail = function () {
	    if (!$scope.isDetailDisabled) {
	        $scope.desactiveAll();
	        $scope.isActiviteDisabled = true;
	        $scope.isDetailCurrent = true;
	        $location.path('/details');
	    }

	};


	$scope.etapeOffre = function () {
	    if (!$scope.isOffreDisabled) {
	        $scope.desactiveAll();
	        $scope.isActiviteDisabled = true;
	        $scope.isDetailDisabled = true;
	        $scope.isOffreCurrent = true;
	        $location.path('/offre');
	    }

	};

	$scope.etapeFin = function () {
	    $scope.desactiveAll();
	    $scope.isActiviteDisabled = true;
	    $scope.isDetailDisabled = true;
	    $scope.isOffreDisabled = true;
	    $scope.isFinCurrent = true;
	    $location.path('/fin');

	};


	$scope.basculeAccueil = function (value) {
	    $scope.desactiveAll();
	    $scope.isActiviteCurrent = true;
	    $scope.isCalendarHidden = false;
	    $scope.name = '';
	    $location.path('/accueil');
	};


	$scope.afficheContract = function (value) {
	    $scope.isContratHidden = false;
	};

 $scope.goNext = function (hash) {
        $location.path(hash);
    };




    /**
     * Supplies a function that will continue to operate until the
     * time is up.
     */
    function debounce(func, wait, context) {
      var timer;

      return function debounced() {
        var context = $scope,
            args = Array.prototype.slice.call(arguments);
        $timeout.cancel(timer);
        timer = $timeout(function() {
          timer = undefined;
          func.apply(context, args);
        }, wait || 10);
      };
    }

    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildDelayedToggler(navID) {
      return debounce(function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }, 200);
    }

    function buildToggler(navID) {
      return function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }
    }
  })
  .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });

    };
  })
  .controller('RightCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      $mdSidenav('right').close()
        .then(function () {
          $log.debug("close RIGHT is done");
        });
    };
  });