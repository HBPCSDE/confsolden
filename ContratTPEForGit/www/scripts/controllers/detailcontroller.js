﻿contrattpe.controller('detailcontroller', ['$scope', function ($scope) {
    $scope.title = 'Details!';
    $scope.paniermoyen = 0;
   
    if ($scope.$parent.activite == 'restaurant') {
        $scope.paniermoyenactivite = 15;
        $scope.paniermoyenmin = 10;
        $scope.paniermoyenmax = 60;
        $scope.firsttitle = [
            { title: 'Caisse fixe', subtitle: 'LES CLIENS REGLENT-ILS AU COMPTOIR ?', pourcentage: 95 },
            { title: 'Caisse déplacement', subtitle: 'FAITES-VOUS DE LA LIVRAISON A DOMICILE?', pourcentage: 60 }
      ];
      
    }
    else if ($scope.$parent.activite == 'hotel') {
        $scope.paniermoyenactivite = 80;
        $scope.paniermoyenmin = 10;
        $scope.paniermoyenmax = 300;
        $scope.firsttitle = [
            { title: 'Caisse fixe', subtitle: 'LES CLIENS REGLENT-ILS AU COMPTOIR ?', pourcentage: 99 },
            { title: 'VAD (site internet)', subtitle: 'FAITES-VOUS DE LA RESERVATION EN LIGNE?', pourcentage: 40 },
            { title: 'Location', subtitle: 'PROPOSEZ-VOUS DE LA LOCATION DE BIENS?', pourcentage: 60 },
            { title: 'Paiement hors zone euro ', subtitle: 'ACCEPTEZ-VOUS DES CLIENTS HORS ZONE EURO?', pourcentage: 70 },
        ];

    }
    else
    {
        $scope.paniermoyenactivite = 100;
        $scope.paniermoyenmin = 10;
        $scope.paniermoyenmax = 500;
        $scope.firsttitle = [
           { title: 'Caisse fixe', subtitle: 'POUR LES BILLETERIES PAR EXEMPLE', pourcentage: 67 },
            { title: 'Caisse mobile', subtitle: 'POUR LES RESTAURANTS PAR EXEMPLE', pourcentage: 95 },
           { title: 'Caisse en déplacement', subtitle: 'TAXI, FOIRE ...', pourcentage: 60 },
           { title: 'Paiement en ligne', subtitle: 'SITE de E-COMMERCE', pourcentage: 6 }
        ];
    }


    $scope.lasttitle = [
   { title: 'Connexion ADSL', subtitle: 'VOTRE COMMERCE EST CONNECTÉ EN ADSL OU FIBRE', pourcentage: 75 },
    { title: 'Abonnement téléphone', subtitle: 'PAYEZ-VOUS UN ABONNEMENT TELEPHONIQUE?', pourcentage: 20 }
    ];
    
}]);