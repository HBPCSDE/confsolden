﻿contrattpe.controller('activitecontroller', ['$scope', function ($scope) {
    //$scope.title = 'Hola!';

    //var delay = 0;
    //$('.flex-item').each(function () {
    //    $(this).animateCSS("fadeInLeft", { delay: delay });
    //    delay += 50;
    //});

    TweenMax.set($('.activites_item'), { autoAlpha: 0 });
    TweenMax.staggerTo($('.activites_item'), 0.1, { autoAlpha: 1 }, 0.08);


    $scope.valueActivite = function (value) {
        $scope.$parent.isCalendarHidden = true;
        $scope.$parent.activite = value;
        $scope.$parent.etapeDetail();
    };
}]);