﻿contrattpe.controller('offrecontroller', ['$scope', function ($scope) {
    $scope.title = 'Offre!';

    $scope.nombreTPE = 0;

    $scope.total = 19;
    $scope.add = function (value, isAdd) {
        if (isAdd) {
            $scope.total += value;
        }
        else {
            $scope.total -= value;
        }
    }
    $scope.frais = 0;
    $scope.addfrais = function (value, isAdd) {
        if (isAdd) {
            $scope.frais += value;
        }
        else {
            $scope.frais -= value;
        }
    }
    $scope.offre1 = [
   { title: 'Souhaitez-vous un lecteur de carte PIN Pad ?', subtitle: 'Le client insère sa carte dans le PIN Pad', pourcentage: 20 }
    ];
    $scope.cartesCB = { title: 'Proximité CB', subtitle: 'Carte de débit et de crédit', pourcentage: 90, statut: true };
    $scope.cartesVISA = { title: 'VISA', subtitle: 'Carte de débit et de crédit', pourcentage: 90, statut: true };
    $scope.cartesMCI = { title: 'MASTERCARD', subtitle: 'Carte de débit et de crédit', pourcentage: 90, statut: true };
    $scope.typecartesCB = [
    { title: 'Cartes Commerciales', subtitle: '', pourcentage: 90, statut: false },
    { title: 'Cartes de débit', subtitle: '', pourcentage: 90, statut: true },
    { title: 'Cartes de crédit', subtitle: '', pourcentage: 90, statut: true }
    ];
    $scope.typecartesVISA = [
    { title: 'Cartes Commerciales', subtitle: '', pourcentage: 90, statut: false },
    { title: 'Cartes de débit', subtitle: '', pourcentage: 90, statut: true },
    { title: 'Cartes de crédit', subtitle: '', pourcentage: 90, statut: true }
    ];
    $scope.typecartesMCI = [
    { title: 'Cartes Commerciales', subtitle: '', pourcentage: 90, statut: false },
    { title: 'Cartes de débit', subtitle: '', pourcentage: 90, statut: true },
    { title: 'Cartes de crédit', subtitle: '', pourcentage: 90, statut: true }
    ];

    $scope.options = [
   { title: 'Union Pay', subtitle: 'Logiciel Union Pay (ex CUP)', pourcentage: 10 },
    { title: 'DCC', subtitle: 'Logiciel DCC (Dollar Américain, Livre Sterling, Fanc Suisse, Yen, Dollar Hong Kong, Rouble', pourcentage: 20 },
    { title: 'AMEX', subtitle: 'Cartes American Express', pourcentage: 30 }
    ];

    $scope.afficheContract = function () {
        $scope.$parent.afficheContract();
    };
}]);