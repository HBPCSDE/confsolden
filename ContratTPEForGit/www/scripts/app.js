﻿/// <reference path="controllers/accueil.html" />
var contrattpe = angular.module('contrattpe', ['ngRoute', 'ngMaterial']);

contrattpe.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
        when('/accueil', { 
            templateUrl: 'scripts/controllers/accueil.html',
            controller: 'accueilcontroller'
        }).
          when('/details', {
              templateUrl: 'scripts/controllers/details.html',
              controller: 'detailcontroller'
          }).
          when('/activite', {
              templateUrl: 'scripts/controllers/activite.html',
              controller: 'activitecontroller'
          }).
          when('/offre', {
              templateUrl: 'scripts/controllers/offre.html',
              controller: 'offrecontroller'
          }).
        otherwise({
            redirectTo: '/accueil'
        }); 
  }]);


contrattpe.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('blue', {
      'default': '800', // by default use shade 400 from the pink palette for primary intentions
      'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
      'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
      'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
    })   ;
});

